$('.form__input').change(function() {
  $(this).addClass('form__input--active');
  if( !$(this).val() ) {
    $(this).removeClass('form__input--active');
  }
});